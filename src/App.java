public class App {
    public static void main(String[] args) throws Exception {
        /*
         * Comment nhiều dòng
         * ví dụ sử dụng biến String
         */
        System.out.println("Hello, World!");
        String strName = new String("Devcamp");
        System.out.println(strName);  //comment 1 dòng : in biến strName ra console
        /*
         * Ví dụ sử dụng các phương thức
         */
        System.out.println(" Chuyển về chữ thường : ToLowerCase " +  strName.toLowerCase());
        System.out.println(" Chuyển về chữ hoa : ToUpperCase " +  strName.toUpperCase());
        System.out.println(" Chiều dài của chuỗi : length " +  strName.length());
    }
}
